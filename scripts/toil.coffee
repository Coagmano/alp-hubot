# Description:
#   Make sure that hubot knows the rules.
#
# Commands:
#   hubot toil me <hours> hours for <reason> - Displays all of the help commands that Hubot knows about.
#   hubot toil show <person | me> - Displays all help commands that match <query>.
#
strftime = require 'strftime'
timestampFormat = "%d/%m%l:%M%P"
mailer = require('nodemailer').createTransport(
  require('nodemailer-mandrill-transport')({
    auth: {
      apiKey: process.env.MANDRILL_API_KEY
    }
  }))

module.exports = (robot) ->

  _toilSend = (req, res) ->
    output = _getToilFormatted()
    res.send output
    mailer.sendMail(
      {
        to: 'fred.stark@cbr.alp.org.au',
        from: 'hubot@australianlabor.org.au',
        subject: 'TOIL for the week',
        text: output
      },
      (err, info) ->
        if (err)
          console.error(err);
        else
          console.log(info);
    )

  _getToilFormatted = (name) ->
    data = robot.brain.get 'toil'
    totalHours = {}
    output = ""

    for own key, records of data
      totalHours[key] ?= 0
      for record in records
        totalHours[key] += record.hours

    if name?
      if data[name]?
        output += "#{name} has #{totalHours[name]}hrs of TOIL accrued for: \n"
        for record in data[name]
          output += "  #{record.timestamp} - #{record.hours}hrs for #{record.reason}\n"
      else
        output = "#{name} has no TOIL saved, or does not exist"
    else
      output += "It's TOIL time!\n"
      for own key, records of data
        output += "#{key} has #{totalHours[key]}hrs of TOIL accrued: \n"
        for record in records
          output += "  #{record.timestamp} - #{record.hours}hrs for #{record.reason}\n"

    output # Return output

  ###
  Toil help
  ###
  robot.respond /toil help/i, (res) ->
    res.send """
             Usage: #{robot.name} toil [action],
                toil me <hours> hours for <reason> - Add toil hours for yourself
                toil show <name | me> - Show someone's toil hours, or your own with 'me'
                toil reset <name | all> - clear the toil counter
                toil send - Send the toil report to Erin
             """
  ###
  Toil me
  Add time to toil record
  ###
  robot.respond /toil (?:me |add )?(\d+) ?(?:hour |hours |hr | hrs)?for (.*)/i, (res) ->
    hours = Number.parseFloat(res.match[1])
    reason = res.match[2]
    user = res.envelope.user.name.toLowerCase()

    # store toil somewhere
    data = robot.brain.get 'toil'
    data ?= {}

    data[user] ?= []
    data[user].push { hours:hours, reason:reason, timestamp:strftime(timestampFormat) }
    robot.brain.set 'toil', data
    robot.brain.save()

    res.send "Added #{hours} to #{user} for #{reason}"

  ###
  TOIL show
  show accumulated time
  ###
  robot.respond /toil show ?(.*)?/i, (res) ->
    name = res.match[1]
    name = name.toLowerCase() unless !name?
    if name == 'me'
      name = res.envelope.user.name.toLowerCase()
    res.send _getToilFormatted(name)

  ###
  TOIL reset
  Reset accumluated toil to 0
  ###
  robot.respond /toil reset (\w*)/i, (res) ->
    name = res.match[1]
    name = name.toLowerCase() unless !name?
    data = robot.brain.get 'toil'
    if name == 'all'
      data = {}
      name = 'everyone'
    else
      data[name] = []
    robot.brain.set 'toil', data
    robot.brain.save()
    res.send "#{name}'s Toil reset"

  ###
  TOIL send
  Send toil email to Erin
  ###
  robot.router.get "/#{robot.name}/toil/send/?", _toilSend
  robot.respond /toil send/, _toilSend
